
#include<stdarg.h>
#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<time.h>
#include<sys/time.h>
#include<sys/file.h>
#include<sys/types.h>
#include<errno.h>
#include<libgen.h>
#include"client.h"

#ifndef LOGLEVEL
#define LOGLEVEL DEBUG
#endif

#define MAXSIZE_LOG 1024*1024//日志文件的大小


//gcc扩展语法,通过s_loginfo数组即可获取等级对应的字符串
static const char* s_loginfo[] = 
{

        [ERROR] = "ERROR",
        [WARN]  = "WARN",
        [INFO]  = "INFO",
        [DEBUG] = "DEBUG",
};

int mylog(const char *function, int line, enum LogLevel level, const char *fmt, ...)//含有可变参数的函数
{
        char           *tmp;
        time_t         t;
        struct tm      *p;
        struct timeval tv;
        int            len;
        char           *progname;
        int            millsec;
        FILE           *fp;
        char           buf[100];
        char           log_buf[200];
        char           time_buf[32];
        va_list        arg_list;
        int            millisec;
        off_t          filesize;
        
        memset(buf,0,sizeof(buf));
        va_start(arg_list,fmt);//指向可变参数表中的第一个参数
        vsnprintf(buf,sizeof(buf),fmt,arg_list);
	
	//获取时间
        t = time(NULL);
        p = localtime(&t);

        gettimeofday(&tv,NULL);
        millisec = (int)(tv.tv_usec/100);

        snprintf(time_buf,32,"%04d-%02d-%02d %02d:%02d:%02d:%03d",p->tm_year+1900,p->tm_mon+1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec,millisec);
        

        if(level > LOGLEVEL)
                return -1;

        

        len = snprintf(log_buf,sizeof(buf),"[%s][%s][%s:%d]:%s\n",time_buf,s_loginfo[level], function, line, buf);
        
        if((fp = fopen("mylog.txt","a+"))==NULL)
        {
                printf("fopen mylog.txt failed:%s\n",strerror(errno));
                return -1;
        }

        
        if(flock(fp->_fileno,LOCK_EX) ==-1 )//文件上锁
        {
            printf("flock LOCK_EX failed:%s\n",strerror(errno));
            return -1;
        }

        if(filesize=lseek(fp->_fileno,0,SEEK_END) ==-1 )//将文件指针偏移到文件尾
        {
            printf("lseek failed:%s\n",strerror(errno));
            return -1;
        }
 
        if(filesize == MAXSIZE_LOG)//判断文件是否为满
        {
            if(ftruncate(fp->_fileno,0) ==-1 )//清空文件
            {
                printf("ftruncate failed:%s\n",strerror(errno));
                return -1;
            }
        }

        fprintf(fp,"%s\n",log_buf);//文件操作，将信息写入日志文件

        va_end(arg_list);

        fclose(fp);

        if(flock(fp->_fileno,LOCK_UN) ==-1 )//文件解锁
        {
            printf("flock LOCK_UN failed:%s\n",strerror(errno));
            return -1;
        }

        return 0;

}
