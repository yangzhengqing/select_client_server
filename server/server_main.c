
#include "server.h"


int main(int argc,char **argv)
{

	int		server_port;
	int		sockfd 	= -1;
	int		clienfd	= -1;
	int		re_val 	= -1;


	//参数解析
	re_val = argument_parse(argc, argv, &server_port);
	if(re_val < 0)
	{
		printf("参数解析错误!\n");
		exit(0);
	}

	//套接字初始化
	re_val = socket_init(server_port,&sockfd);
	if(re_val < 0)
	{
		printf("套接字初始化错误!\n");
	}

	//select服务器开始工作
	server_select_create(sockfd);	

	return 0;
}
